#-------------------------------------------------
#
# Project created by QtCreator 2013-04-25T22:23:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = wolfedit
TEMPLATE = app


SOURCES += main.cpp\
        wolfeditwindow.cpp

HEADERS  += wolfeditwindow.h

FORMS    += wolfeditwindow.ui
